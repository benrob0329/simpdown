#!/usr/bin/env python3
import markdown, os, sys, re, shutil


def updateRepo(link):
    """Updates the repository under site if it exists, clones link if it does not."""
    if "site" in os.listdir(path='.'):
        os.chdir("site")
        os.system("git pull")
        os.chdir('..')
    else:
        os.system("git clone " + link + " site")


def convertLinks(string):
    """Convert all local .md links into local .html links."""
    output = ''
    for line in string.splitlines(True):
        # [any string](anystring.md) -> [any string](anystring.html)
        output += re.sub("\[(.*?)\]\((.+?)\.md(.*?)\)", "[\\1](\\2.html\\3)", line)
    return output


# Convert to HTML
def siteToHTML(template):
    """Convert site/markdown into an HTML stub, then place that into template."""
    ## Walk the site directory and convert the markdown to html
    for root, directories, filenames in os.walk('site/markdown'):
        for filename in filenames:
            targetpath = root.replace("site/markdown", "public")
            os.makedirs(targetpath, mode=0o755, exist_ok=True)
            
            with open(os.path.join(root, filename)) as f:
                source = f.read()
            ## Convert "*.md" links to html links
            source = convertLinks(source)
            ## Get Title From First Header
            title = re.search("# (.*)", source)
            ## Convert Markdown To HTML Stub
            html = markdown.markdown(source)
            ## Place HTML Stub Into Template and Title
            with open(os.path.join(targetpath, filename.replace("md", "html")), "w") as f:
                output = template.replace("<<body>>", html)
                if "<<title>>" in output and title:
                    output = output.replace("<<title>>", title.group(1))
                f.write(output)


# Remove the old generation
if "public" in os.listdir():
    shutil.rmtree("public")
# Update the repository according to the first argument
updateRepo(sys.argv[1])

# Import the template
with open("site/template.html", 'r') as f:
    template = f.read()

# Convert the markdown to HTML
siteToHTML(template)

# Copy the CSS files
if "assets" in os.listdir("site"):
    shutil.copytree("site/assets", "public/assets")
