# Simpdown
The Simple Markdown Site Generator.

## Installation
Simply clone the repo into a directory of your choice, only dependancies are ``git`` and ``python3-markdown``.

## Usage
```
$ /path/to/simpdown/main.py "https://link.to/repo.git"
```

The Git repo should have a structure like so:
```
.
├── assets
│   └── index.css
├── markdown
│   ├── subfolder
│   │   ├── bar.md
│   │   └── baz.md
│   └── index.md
└── template.html
```

Of course, the names of files can vary, only ``template.html``, the ``assets`` directory, and ``index.md`` are required.

The ``template.html`` should contain the header, css inclusions, and basic body structure with ``<<body>>`` being put in place of the actual content.

### Example
https://gitlab.com/benrob0329/totallynotashadyweb.site/-/tree/e2b11e551c6e28ad499d908f54ff1595816bf7c3

### Goals

- Be as simple as possible to set up and deploy
- Be flexible for site layout and options
